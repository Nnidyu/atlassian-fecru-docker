#!/bin/bash

set -e

for i in config.xml var lib syntax
do
	if [[ ! -e ${FECRU_HOME}/$i ]]
	then
		cp -afv ${FECRU_INSTALL}/$i ${FECRU_HOME}/
	fi
done

if [[ -d ${FECRU_HOME}/overlay ]]
then
	cp -afv ${FECRU_HOME}/overlay/. ${FECRU_INSTALL}/
fi

/bin/bash -c "${FECRU_INSTALL}/bin/run.sh"
