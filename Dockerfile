FROM nnidyu/centos-oracle-java
MAINTAINER Mateusz Małek <mmalek@iisg.agh.edu.pl>

ENV FECRU_VERSION 3.7.0
ENV FECRU_HOME /opt/atlassian/fecru/home
ENV FECRU_INSTALL_BASE /opt/atlassian/fecru
ENV FECRU_INSTALL ${FECRU_INSTALL_BASE}/install

RUN mkdir -p $FECRU_INSTALL_BASE && \
	cd /tmp && \
	wget https://downloads.atlassian.com/software/crucible/downloads/crucible-${FECRU_VERSION}.zip && \
	unzip crucible-${FECRU_VERSION}.zip && \
	mv fecru-$FECRU_VERSION $FECRU_INSTALL && \
	rm crucible-${FECRU_VERSION}.zip && \
	/usr/sbin/useradd --home-dir $FECRU_HOME --shell /bin/bash fecru && \
	chown -R fecru:fecru $FECRU_INSTALL

ENV FISHEYE_INST $FECRU_HOME

VOLUME ["$FECRU_HOME"]

EXPOSE 8060

ADD ./entrypoint.sh /entrypoint.sh

WORKDIR $FECRU_HOME
USER fecru

ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
